import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { AuthConfigCallback } from '@ngauth/core';
import { AuthConfigManager } from '@ngauth/services';


// This component is primarily to be used for debugging purposes, during development.
// It shows an info regarding whether the authconfig has been loaded.
// It also gives an option to explicitly trigger loading auth config.
// (Note that this needs to be generally done when an app is loaded.)

@Component({
  selector: 'ngauth-auth-ready',
  templateUrl: './auth-ready.component.html',
  styleUrls: ['./auth-ready.component.css']
})
export class AuthReadyComponent implements OnInit, AuthConfigCallback {

  // temporary
  isAuthReady: boolean = false;
  chipLabel: string = "";
  // buttonLabel: string = "";

  constructor(
    private formBuilder: FormBuilder,
    private authConfigManager: AuthConfigManager
  ) {
  }

  public refreshUI() {
    this._updateChipUI();
  }

  private _updateChipUI() {
    if (this.authConfigManager.isLoaded) {
      this.isAuthReady = true;
      this.chipLabel = "Auth Ready";
      // this.buttonLabel = "-";   // Just hide the button?
    } else {
      this.isAuthReady = false;
      this.chipLabel = "No Auth";
      // this.buttonLabel = "Start Auth";
    }
  }

  ngOnInit() {
    this._updateChipUI();
    // this.loadAuthConfig();
    this.authConfigManager.addCallback(this);
  }

  authConfigLoaded(): void {
    if(isDL()) dl.log(">>> authConfig loaded.");
    this._updateChipUI();
  }

  loadAuthConfig() {
    // this.authConfigManager.loadConfig().subscribe((authConfig) => {
    //   if(dl.isLoggable()) dl.log(">>> authConfig loaded.");
    //   this._updateChipUI();
    // });
    this.authConfigManager.triggerLoading();
  }

}
