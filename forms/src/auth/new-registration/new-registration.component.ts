import { Component, OnInit, EventEmitter, Input, Output } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { CognitoResult, UserRegistrationInfo } from '@ngauth/core';
import { DialogParentCallback } from '@ngauth/core';
import { DefaultAuthRoutes } from '@ngauth/core';
import { IUserRegistrationService } from '@ngauth/services';
import { UserAuthServiceFactory } from '@ngauth/services';
import { DialogBaseComponent } from '../../base/dialog-base-component';


/**
 * This component is responsible for displaying and controlling
 * the registration of the user.
 */
@Component({
  selector: 'ngauth-new-registration',
  templateUrl: './new-registration.component.html',
  styleUrls: ['./new-registration.component.css'],
})
export class NewRegistrationComponent extends DialogBaseComponent implements OnInit {

  @Output() onUserRegistered = new EventEmitter<CognitoResult>();

  // TBD:
  @Input("min-password") minPasswordLength: number;

  registrationUser: UserRegistrationInfo;
  errorMessage: string;

  registerForm: FormGroup;
  errorTips: { [name: string]: string };

  private registrationService: IUserRegistrationService;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authRoutes: DefaultAuthRoutes,
    private userAuthServiceFactory: UserAuthServiceFactory
  ) {
    super();
    if(isDL()) dl.log("NewRegistrationComponent constructor");

    this.registrationService = this.userAuthServiceFactory.getUserRegistrationService();

    this._buildForm();
  }

  private _buildForm() {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.minLength(2), Validators.required])],
      email: ['', Validators.compose([Validators.email, Validators.required])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(32), Validators.required])],
    });
    this.errorTips = {
      name: "Name should be a minimum of 2 character",
      email: "Email address is invalid",
      password: "Password should be between 6 and 32 characters"
    }
  }
  ngOnChanges() {
    this.registerForm.reset({
      // tbd: use a data model with init values...
      name: '',
      email: '',
      password: ''
    });
  }
  onSubmit() {
    if(isDL()) dl.log("onSubmit() called.");

    // TBD: save
    this.registrationUser.name = this.registerForm.controls['name'].value;
    this.registrationUser.email = this.registerForm.controls['email'].value;
    this.registrationUser.password = this.registerForm.controls['password'].value;
    if(isDL()) dl.log(">>> registrationUser = " + this.registrationUser);
    this.doRegister();

    // this.ngOnChanges();  // ???
    this.callCallback('submit');
  }
  revert() // revert or clear?
  {
    if(isDL()) dl.log("revert() called.");
    this.ngOnChanges();
    this.callCallback('revert');
  }
  cancel() // dismiss
  {
    if(isDL()) dl.log("cancel() called.");
    this.ngOnChanges();
    this.callCallback('cancel');
  }



  ngOnInit(): void {
    this.registrationUser = new UserRegistrationInfo();
    this.errorMessage = null;
  }


  // tbd:
  doRegister() {
    this.errorMessage = null;
    this.registrationService.register(this.registrationUser).subscribe((cognitoResult: CognitoResult) => {
      if (cognitoResult.message) { // error
        this.errorMessage = cognitoResult.message;
        if(dl.isLoggable()) dl.log("errorMessage: " + this.errorMessage);
      } else { // success
        // //move to the next step
        // if(dl.isLoggable()) dl.log("redirecting");
        // // tbd:
        // this.router.navigate([this.authRoutes.authRouteInfo.confirm, result.user.username]);
        // // ...
      }

      // tbd:
      this.onUserRegistered.emit(cognitoResult);
      // ...
    });
  }

}
