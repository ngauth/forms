import { Component, OnInit, OnDestroy, EventEmitter, Input, Output } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { LoggedInStatus } from '@ngauth/core';
import { DefaultAuthRoutes } from '@ngauth/core';
import { IUserLoginService } from '@ngauth/services';
import { UserAuthServiceFactory } from '@ngauth/services';


@Component({
  selector: 'ngauth-user-logout',
  template: ''
})
export class UserLogoutComponent implements OnInit {

  @Output() onUserLoggedOut = new EventEmitter<LoggedInStatus>();
  
  private loginService: IUserLoginService;

  constructor(
    private router: Router,
    private authRoutes: DefaultAuthRoutes,
    private userAuthServiceFactory: UserAuthServiceFactory
  ) {
    if(isDL()) dl.log("UserLogoutComponent constructor");

    this.loginService = this.userAuthServiceFactory.getUserLoginService();

  }

  ngOnInit(): void {
    this.loginService.isAuthenticated().subscribe((status: LoggedInStatus) => {
      if(dl.isLoggable()) dl.log("status.loggedIn = " + status.loggedIn);
      // if (status.loggedIn) {
      //   this.userLoginService.logout();
      //   // tbd:
      //   this.router.navigate([this.authRoutes.authRouteInfo.home]);
      //   // ...
      // }
  
      // // tbd:
      // this.router.navigate([this.authRoutes.authRouteInfo.home]);
      // // ...

      // ???
      this.onUserLoggedOut.emit(status);
    });
  }


  logout() {
    // this.errorMessage = null;
    this.loginService.logout();  // Does it always work? What happens if the logout fails?
    let status = new LoggedInStatus(null, false);
    this.onUserLoggedOut.emit(status);
  }


  // isLoggedIn(message: string, isLoggedIn: boolean) {
  //   if (isLoggedIn) {
  //     this.userLoginService.logout();
  //     // tbd:
  //     this.router.navigate([this.authRoutes.authRouteInfo.home]);
  //     // ...
  //   }

  //   // tbd:
  //   this.router.navigate([this.authRoutes.authRouteInfo.home]);
  //   // ...
  // }
}
