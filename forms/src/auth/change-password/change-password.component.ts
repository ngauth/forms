import { Component, OnInit, EventEmitter, Input, Output } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { LoggedInStatus, CognitoResult, NewPasswordInfo } from '@ngauth/core';
import { DialogParentCallback } from '@ngauth/core';
import { DefaultAuthRoutes } from '@ngauth/core';
import { IUserRegistrationService, IUserLoginService } from '@ngauth/services';
import { UserAuthServiceFactory } from '@ngauth/services';
import { DialogBaseComponent } from '../../base/dialog-base-component';


@Component({
  selector: 'ngauth-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css'],
})
export class ChangePasswordComponent extends DialogBaseComponent implements OnInit {

  @Output() onPasswordChanged = new EventEmitter<CognitoResult>();

  // TBD:
  @Input("min-password") minPasswordLength: number;

  registrationUser: NewPasswordInfo;
  errorMessage: string;

  changeForm: FormGroup;
  errorTips: { [name: string]: string };

  private registrationService: IUserRegistrationService;
  private loginService: IUserLoginService;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authRoutes: DefaultAuthRoutes,
    private userAuthServiceFactory: UserAuthServiceFactory
  ) {
    super();
    if(isDL()) dl.log("ChangePasswordComponent constructor");

    this.registrationService = this.userAuthServiceFactory.getUserRegistrationService();
    this.loginService = this.userAuthServiceFactory.getUserLoginService();

    this._buildForm();
    this.registrationUser = new NewPasswordInfo();
    this.errorMessage = null;
  }

  private _buildForm() {
    this.changeForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.email, Validators.required])],
      oldPassword: ['', Validators.compose([Validators.minLength(6), Validators.required])],
      newPassword: ['', Validators.compose([Validators.minLength(6), Validators.required])],
    });
    this.errorTips = {
      email: "Email address is invalid",
      oldPassword: "Password is a miminum of 6 characters",
      newPassword: "Password is a miminum of 6 characters",
    }
  }
  ngOnChanges() {
    this.changeForm.reset({
      // tbd: use a data model with init values...
      email: '',
      oldPassword: '',
      newPassword: '',
    });
  }
  onSubmit() {
    if(isDL()) dl.log("onSubmit() called.");

    // TBD: save
    this.registrationUser.username = this.changeForm.controls['email'].value;
    this.registrationUser.oldPassword = this.changeForm.controls['oldPassword'].value;
    this.registrationUser.newPassword = this.changeForm.controls['newPassword'].value;
    this.errorMessage = null;
    this.loginService.changePassword(this.registrationUser).subscribe((cognitoResult) => {
      if (cognitoResult.message) { // error
        this.errorMessage = cognitoResult.message;
        if(dl.isLoggable()) dl.log("errorMessage: " + this.errorMessage);
      } else { // success
        // //move to the next step
        // if(dl.isLoggable()) dl.log("redirecting");
        // // tbd:
        // this.router.navigate([this.authRoutes.authRouteInfo.home]);
        // // ...
      }

      // tbd:
      this.onPasswordChanged.emit(cognitoResult);
      // ...
    });

    // this.ngOnChanges();  // ???
    this.callCallback('submit');
  }
  revert() // revert or clear?
  {
    if(isDL()) dl.log("revert() called.");
    this.ngOnChanges();
    this.callCallback('revert');
  }
  cancel() // dismiss
  {
    if(isDL()) dl.log("cancel() called.");
    this.ngOnChanges();
    this.callCallback('cancel');
  }


  ngOnInit() {
    this.errorMessage = null;
    // TBD:
    // This does not make sense...
    if(isDL()) dl.log("Checking if the user is already authenticated. If so, then redirect to the secure site");
    this.loginService.isAuthenticated().subscribe((status: LoggedInStatus) => {
      if(dl.isLoggable()) dl.log("status.loggedIn = " + status.loggedIn);
      if (status.loggedIn) {
        // tbd:
        // this.router.navigate([this.authRoutes.authRouteInfo.home]);
        // ...

        // tbd:
        // Hide the email form field
        // and use email from the logged-in account
        // ....
      } else {
        // ??
      }
    });
  }


  // onRegister() {
  //   if(isDL()) dl.log(this.registrationUser);
  //   this.errorMessage = null;
  //   this.registrationService.newPassword(this.registrationUser, this);
  // }


  // To be deleted:

  // cognitoCallback(message: string, result: any) {
  //   if (message != null) { //error
  //     this.errorMessage = message;
  //     if(isDL()) dl.log("result: " + this.errorMessage);
  //   } else { //success
  //     //move to the next step
  //     if(isDL()) dl.log("redirecting");
  //     // tbd:
  //     this.router.navigate([this.authRoutes.authRouteInfo.home]);
  //     // ...
  //   }
  // }

  // isLoggedIn(message: string, isLoggedIn: boolean) {
  //   if (isLoggedIn) {
  //     // tbd:
  //     this.router.navigate([this.authRoutes.authRouteInfo.home]);
  //     // ...
  //   }
  // }

}
