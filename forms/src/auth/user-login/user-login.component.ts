import { Component, OnInit, EventEmitter, Input, Output } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { LoggedInStatus, CognitoResult } from '@ngauth/core';
import { DialogParentCallback } from '@ngauth/core';
import { DefaultAuthRoutes } from '@ngauth/core';
import { IUserLoginService } from '@ngauth/services';
import { UserAuthServiceFactory } from '@ngauth/services';
import { DialogBaseComponent } from '../../base/dialog-base-component';

// import { DdbServiceUtil } from '../../services/cognito/ddb-service-util';


@Component({
  selector: 'ngauth-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css'],
})
export class UserLoginComponent extends DialogBaseComponent implements OnInit {

  @Output() onUserLoggedIn = new EventEmitter<CognitoResult>();

  // TBD:
  @Input("min-password") minPasswordLength: number;

  email: string;
  password: string;
  errorMessage: string;

  loginForm: FormGroup;
  errorTips: { [name: string]: string };

  private loginService: IUserLoginService;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authRoutes: DefaultAuthRoutes,
    // private ddbUtil: DdbServiceUtil,
    private userAuthServiceFactory: UserAuthServiceFactory
  ) {
    super();
    console.log("UserLoginComponent constructor");

    this.loginService = this.userAuthServiceFactory.getUserLoginService();

    this._buildForm();
  }

  private _buildForm() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.email, Validators.required])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],

    });
    this.errorTips = {
      email: "Email address is invalid",
      password: "Password is a miminum of 6 characters"
    }
  }
  ngOnChanges() {
    this.loginForm.reset({
      // tbd: use a data model with init values...
      email: '',
      password: ''
    });
  }
  onSubmit() {
    console.log("onSubmit() called.");

    // TBD: save
    this.email = this.loginForm.controls['email'].value;
    this.password = this.loginForm.controls['password'].value;
    console.log(">>> email = " + this.email + "; password.length = " + this.password.length);
    this.doLogin();

    // this.ngOnChanges();  // ???
    this.callCallback('submit');
  }
  revert() // revert or clear?
  {
    console.log("revert() called.");
    this.ngOnChanges();
    this.callCallback('revert');
  }
  cancel() // dismiss
  {
    console.log("cancel() called.");
    this.ngOnChanges();
    this.callCallback('cancel');
  }

  ngOnInit() {
    this.errorMessage = null;
    console.log("Checking if the user is already authenticated. If so, then redirect to the secure site");
    this.loginService.isAuthenticated().subscribe((status: LoggedInStatus) => {
      console.log("status.loggedIn = " + status.loggedIn);
      if (status.loggedIn) {
        // tbd:
        // this.router.navigate([this.authRoutes.authRouteInfo.home]);
        // ...
      } else {
        // ??
      }
    });
  }

  // tbd:
  doLogin() {
    this.errorMessage = null;
    this.loginService.authenticate(this.email, this.password).subscribe((cognitoResult: CognitoResult) => {
      // ???
      // if (cognitoResult.message) { //error
      //   this.errorMessage = cognitoResult.message;
      //   console.log("result: " + this.errorMessage);
      //   if (this.errorMessage === 'User is not confirmed.') {
      //     console.log("redirecting");
      //     // tbd:
      //     this.router.navigate([this.authRoutes.authRouteInfo.confirm, this.email]);
      //     // ...
      //   } else if (this.errorMessage === 'User needs to set password.') {
      //     console.log("redirecting to set new password");
      //     // tbd:
      //     this.router.navigate([this.authRoutes.authRouteInfo.change]);
      //     // ...
      //   }
      // } else { //success
      //   this.ddbUtil.writeLogEntry("login");
      //   // tbd:
      //   this.router.navigate([this.authRoutes.authRouteInfo.home]);
      //   // ...
      // }
 
      // tbd:
      let success = !cognitoResult.message;  // What about result???
      console.log(`loginService.authenticate() called. success = ${success}.`);
      this.onUserLoggedIn.emit(cognitoResult);
      // ...
    });
  }


  // onLogin() {
  //   if (this.email == null || this.password == null) {
  //     this.errorMessage = "All fields are required";
  //     return;
  //   }
  //   this.errorMessage = null;
  //   this.userLoginService.authenticate(this.email, this.password, this);
  // }


  // cognitoCallback(message: string, result: any) {
  //   console.log(">>> cognitoCallback()");

  //   if (message != null) { //error
  //     this.errorMessage = message;
  //     console.log("result: " + this.errorMessage);
  //     if (this.errorMessage === 'User is not confirmed.') {
  //       console.log("redirecting");
  //       // tbd:
  //       this.router.navigate([this.authRoutes.authRouteInfo.confirm, this.email]);
  //       // ...
  //     } else if (this.errorMessage === 'User needs to set password.') {
  //       console.log("redirecting to set new password");
  //       // tbd:
  //       this.router.navigate([this.authRoutes.authRouteInfo.change]);
  //       // ...
  //     }
  //   } else { //success
  //     this.ddbUtil.writeLogEntry("login");
  //     // tbd:
  //     this.router.navigate([this.authRoutes.authRouteInfo.home]);
  //     // ...
  //   }
  // }

  // // LoggedInCallback.
  // isLoggedIn(message: string, isLoggedIn: boolean) {
  //   if (isLoggedIn) {
  //     this.router.navigate([this.authRoutes.authRouteInfo.home]);
  //   }
  // }
}
