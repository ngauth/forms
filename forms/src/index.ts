import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { RouterModule } from '@angular/router';

import { NgCoreCoreModule } from '@ngcore/core';
import { NgAuthCoreModule } from '@ngauth/core';
import { NgAuthServicesModule } from '@ngauth/services';

// import { NgAuthMaterialModule } from './ngauth-material.module';
// import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatIconModule,
  MatButtonModule,
  MatChipsModule,
  MatTabsModule,
  MatSidenavModule,
  MatToolbarModule,
  MatMenuModule,
  MatCardModule,
  MatDialogModule,
  MatTooltipModule,
  MatSnackBarModule,
  MatInputModule,
  MatFormFieldModule,
  MatGridListModule,
  MatStepperModule
} from '@angular/material';

// // import { AuthRouteInfo } from './route/auth-route-info';
// import { DefaultAuthRoutes } from './route/default-auth-routes';
// import { DefaultCognitoAuthConf } from './config/default-cognito-auth-conf';

// import { DialogBaseComponent } from './base/dialog-base-component';
import { AuthReadyComponent } from './basic/auth-ready/auth-ready.component';
import { UserStateComponent } from './auth/user-state/user-state.component';
import { NewRegistrationComponent } from './auth/new-registration/new-registration.component';
import { ConfirmRegistrationComponent } from './auth/confirm-registration/confirm-registration.component';
import { ResendCodeComponent } from './auth/resend-code/resend-code.component';
import { UserLoginComponent } from './auth/user-login/user-login.component';
import { UserLogoutComponent } from './auth/user-logout/user-logout.component';
import { ChangePasswordComponent } from './auth/change-password/change-password.component';
import { ResetPasswordStep1Component, ResetPasswordStep2Component } from './auth/reset-password/reset-password.component';

export * from './base/dialog-base-component';
export * from './basic/auth-ready/auth-ready.component';
export * from './auth/user-state/user-state.component';
export * from './auth/new-registration/new-registration.component';
export * from './auth/confirm-registration/confirm-registration.component';
export * from './auth/resend-code/resend-code.component';
export * from './auth/user-login/user-login.component';
export * from './auth/user-logout/user-logout.component';
export * from './auth/change-password/change-password.component';
export * from './auth/reset-password/reset-password.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    // RouterModule,
    // // RouterModule.forRoot([{ path: '', component: SecureHomeComponent }]),
    NgCoreCoreModule.forRoot(),
    NgAuthCoreModule.forRoot(),
    NgAuthServicesModule.forRoot(),
    // NgAuthMaterialModule,
    // NoopAnimationsModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatButtonModule,
    MatChipsModule,
    MatTabsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatMenuModule,
    MatCardModule,
    MatDialogModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatInputModule,
    MatFormFieldModule,
    MatGridListModule,
    MatStepperModule
  ],
  declarations: [
    // DialogBaseComponent,
    AuthReadyComponent,
    UserStateComponent,
    NewRegistrationComponent,
    ConfirmRegistrationComponent,
    ResendCodeComponent,
    UserLoginComponent,
    UserLogoutComponent,
    ChangePasswordComponent,
    ResetPasswordStep1Component, 
    ResetPasswordStep2Component
  ],
  exports: [
    // DialogBaseComponent,
    AuthReadyComponent,
    UserStateComponent,
    NewRegistrationComponent,
    ConfirmRegistrationComponent,
    ResendCodeComponent,
    UserLoginComponent,
    UserLogoutComponent,
    ChangePasswordComponent,
    ResetPasswordStep1Component, 
    ResetPasswordStep2Component
  ]
})
export class NgAuthFormsModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: NgAuthFormsModule,
      providers: [
      ]
    };
  }
}
