# @ngauth/forms

## Overview

`NgAuth` is a suite of Angular modules
comprising a set of auth-related reusable components and services.
The library is mostly frontend/UI, and it uses AWS Cognito for serverless backend.
The `NgAuth - Forms` module
includes a number of UI components for handling user login/logout
and new user registration and confirmation, etc.
The module uses Angular reactive forms.



_(Note: this library is currently in beta,_
_and the APIs may change over time.)_




## Installation

To install this library, run:

```bash
$ npm install --save @ngauth/forms @ngauth/services @ngauth/core
```


Note that the ngauth components use `@angular/material`,
and if you don't enable angular material in your app,
these components will not work properly.

For angular material setup instructions, refer to this document:

* [Getting Started with Angular Material](https://gitlab.com/angularmaterial/setup)




## Using the library

From your Angular `AppModule`:

```typescript
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

// Import the library
import { NgAuthCoreModule } from '@ngauth/core';
import { NgAuthServicesModule } from '@ngauth/services';
import { NgAuthFormsModule } from '@ngauth/forms';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,

    // Specify the library as an import
    NgAuthCoreModule.forRoot(),
    NgAuthServicesModule.forRoot(),
    NgAuthFormsModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

Once the library is imported, you can use its components, directives and pipes in your Angular application:

```xml
<!-- You can now use the library component in app.component.html -->
<h1>
  {{title}}
</h1>
<ngauth-auth-ready></ngauth-auth-ready>
```


## License

MIT © [Harry Y](mailto:sidewaybot@gmail.com)
